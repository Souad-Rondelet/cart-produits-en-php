<?php
session_start();
try{

    $pdo = new PDO('mysql:host=localhost;dbname=visiteur;charset=utf8', 'root', '');
}catch(PDOException $exception){
    die($exception->getMessage());
}

if (isset($_POST["Ajouter"])){

    if (isset($_SESSION["cart"])){

        $id_element = array_column($_SESSION["cart"],"product_id");
        if (!in_array($_GET["id"],$id_element)){
            $count = count($_SESSION["cart"]);
            $item_array = ([

                'product_id' => $_GET["id"],
                'item_name' => $_POST["hidden_titre"],
                'product_price' => $_POST["hidden_price"],
                'item_quantity' => $_POST["quantite"],
            ]);
       
            
            $_SESSION["cart"][$count] = $item_array;
            echo '<script>window.location="Cart.php"</script>';
        }else{
            echo '<script>alert("Le produit est déjà ajouté au panier modifier la quantite")</script>';
            echo '<script>window.location="Cart.php"</script>';
        }
    }else{
        $item_array = ([

            'product_id' => $_GET["id"],
            'item_name' => $_POST["hidden_titre"],
            'product_price' => $_POST["hidden_price"],
            'item_quantity' => $_POST["quantite"],
        ]);
        
        $_SESSION["cart"][0] = $item_array;
    }
}
if (isset($_GET["action"])){
    if ($_GET["action"] == "delete"){
        foreach ($_SESSION["cart"] as $keys => $values){
            if ($values["product_id"] == $_GET["id"]){
                unset($_SESSION["cart"][$keys]);
                echo '<script>alert("Le produit est!")</script>';
                echo '<script>window.location="Cart.php"</script>';
            }
        }
    }
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css"/>
    <link rel="stylesheet" href="Calalogue.css"/>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <title>Document</title>
</head>

<body>
      <div class="box">
        <a id="lien" href="#catalogue">Catalogue</a>
        <a id="lien" href="#catalogue"><i class="icon-cart"></i></a>
       

      </div>


    <div id="catalogue">Liste de catalogue des Produits</div>

    
<?php 
$req = $pdo->prepare('SELECT * FROM produits ORDER BY id ASC');
$req->execute([]);
$resultats = $req->fetchAll(PDO::FETCH_ASSOC);
?>
<?php
foreach ($resultats as $resultat) {?>
   
    
   <form method="post" action="Cart.php?action=Ajouter&id=<?php echo $resultat["id"]; ?>">
    <div class="box1">
    <div class="titreimage">
    <h2 class="titttle"><?php echo $resultat['titre']; ?></h2>
    <img class="imagCasc" src="<?php echo $resultat['image']; ?>"/>
    </div>  
    <div class="box2">
    
    <div id="des">
    <p class="description"><?php echo $resultat['description']; ?></p>
    </div>
    <div class="type"> <?php echo $resultat['type']; ?></div>
    <div class="prix"> <?php echo  number_format($resultat['prix'], 2, ',', ' ');?>€</div>
    

    <input type="text" name="quantite" class="buttton" class="form-control" value="1">
    <input type="hidden" name="hidden_titre"  value="<?php echo $resultat["titre"]; ?>">
    <input type="hidden" name="hidden_price"  value="<?php echo $resultat["prix"]; ?>">
    <input type="submit" name="Ajouter" class="button" value="Ajouter au panier" />

   
    
</div>
</div>
</form>
    <?php
}
?>
<div style="clear: both"></div>
<h3 class="tit">Panier details</h3>

    <table class="tableau table-bordered">
    <tr>
        <th width="10%">Nom de produit</th>
        <th width="5%">Quantité</th>
        <th width="6%">Prix de produit</th>
        <th width="7%">Totale de prix</th>
        <th width="8%">Supprimer les articles</th>
    </tr>


    <?php
    if(!empty($_SESSION["cart"])){
        $total = 0;
        foreach ($_SESSION["cart"] as $keys => $values) {
            ?>
            <tr>
                <td><?php echo $values["item_name"]; ?></td>
                <td><?php echo $values["item_quantity"]; ?></td>
                <td>€ <?php echo $values["product_price"]; ?></td>
                <td>
                    € <?php echo number_format($values["item_quantity"] * $values["product_price"], 2, ',', ' '); ?></td>
                    <td><button type="submit" value="supprimer"><a href="Cart.php?action=delete&id=<?php echo $values["product_id"]; ?>">Supprimer</a>
                    </button></td>
                    <style>
         button {
          background-color:#e60000;
          color: white;
          border: 1px solid red;
          border-radius: 4px;
        
          
        }
        button a {
            color: white;
            text-decoration: none;
            padding: 8px;
        }
        button a:hover{
            color: white;
            text-decoration: none;
        }
       
    </style>
               
            </tr>


           <?php
           $total = $total + ($values["item_quantity"] * $values["product_price"]);
                }
         ?>
                    <tr>
                        <td colspan="3" align="right">Total final</td>
                        <th align="right">€ <?php echo number_format($total, 2, ',', ' '); ?></th>
                        
                    </tr>
                <?php
                }
            ?>
        </table>
        <div>
        <input type="submit" name="Ajouter" class="commande" value="Valider ma commande" />
        </div>
        <div>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. 
            Quidem, veniam veritatis debitis molestias labore consequatur 
            recusandae temporibus et maiores natus illum quaerat nihil at
            dicta minima aliquam est consectetur sapiente illo corporis exercitationem. 
            Optio mollitia molestiae, minima nam sit nihil ex sapiente. Possimus autem 
            doloribus eaque omnis et obcaecati dicta?
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio quia, consectetur vel, sequi ipsam illum nisi at pariatur natus cum doloribus fuga fugit nostrum iure hic omnis possimus beatae minus quisquam veniam facilis? Ipsa alias quam consequatur sunt. Quidem molestias non cumque ipsam odio repellendus minus eius dolorum officia culpa!
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Inventore quibusdam enim quaerat repudiandae maxime laborum quo mollitia, id impedit rerum quisquam nisi saepe cupiditate delectus, dolor quae? At, ea quaerat dolores cum adipisci, expedita porro modi quibusdam molestias non accusamus, debitis explicabo obcaecati sunt ad cupiditate earum reiciendis quas voluptatum.
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Non blanditiis eos hic aut ducimus ipsa voluptates praesentium esse dolorem sunt natus corporis veritatis labore id quae, eius in! Libero blanditiis quis labore eos consectetur tenetur tempora architecto iusto! Voluptates iure qui quas temporibus, unde minus tempore in pariatur aperiam eum.

        </div>

</body>
</html>   
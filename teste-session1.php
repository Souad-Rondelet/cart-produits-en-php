<?php 
session_status();

$_SESSION['color'] = "yellow";
echo $_SESSION['color'] . '<br/>';
$_SESSION['name'] = "steve";
echo $_SESSION['name'];


$value = "John";
setcookie("user", $value, time() + (86400 * 30), '/'); 

if(isset($_COOKIE['user'])) {
  echo "Value is: ". $_COOKIE['user'];
}
//Outputs "Value is: John"
?>
